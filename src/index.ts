import {MyType} from "./types/MyType";

const myTypeValue: MyType = {
    myNumber: 42,
    myString: "foo"
};

console.log("MyTypeValue is: ");
console.log(myTypeValue);